import type Browser, { Tabs } from "webextension-polyfill";

export declare global {
    // const browser: Browser;

    interface Window {
        hasGolorRun: boolean;
        browser: Browser.Browser;
        chrome: Browser.Browser;
    }

    namespace I {
        interface IAlertMessage {
            title?: string;
            message?: string;
            type?: "success" | "error" | "warning";
        }

        // base model---------------------------------
        interface IBaseModel {
            id: string;
            _dateCreated?: number;
            _dateUpdated?: number;
            _dateDeleted?: number;
        }

        interface ILanguage {
            code2: string;
            code3: string;
            translations: Record<string, { label: string }>;
        }

        interface ILabel {
            languageCode2: string;
            translations: Record<string, any>;
        }

        interface IBrowser extends Browser.Browser {}
    }
}

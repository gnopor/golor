/**
 * - Need these 2 variables 'browser' & 'chrome' to build the browser api (API) of the content script
 * - It is needed to not get the error: 'browser'(or'chrome') is not defined
 */
var browser: I.IBrowser = typeof window.browser != "undefined" ? window.browser : window.chrome;
var chrome = browser;

var STAT_CAPTURE_ACTION = "startcapture";
var MOUSE_MOVE_ACTION = "mousemove";
var NEW_COLOR_ACTION = "newcolor";
var LANGUAGE_CHANGE_ACTION = "languagechange";

const CONTENT_SCRIPT_PATH = "/dist/content/script.js";
const CONTENT_STYLE_PATH = "/dist/content/style.css";

const popupLabelsMap = {
    en: {
        languageCode2: "en",
        data: {
            pageTitle: "",
            sections: {
                mainForm: {
                    buttons: { pickColor: { label: "Pick color" } }
                }
            }
        }
    },
    fr: {
        languageCode2: "fr",
        data: {
            pageTitle: "",
            sections: {
                mainForm: {
                    buttons: { pickColor: { label: "Choisir la couleur" } }
                }
            }
        }
    }
};

const popupBody = document.querySelector<HTMLElement>("#popup-body");
const errorContent = document.querySelector<HTMLElement>("#error-content");
const startButton = document.querySelector<HTMLElement>(".start_button");
const colorPreview = document.querySelector<HTMLElement>(".color");
const rgbPreview = document.querySelector<HTMLElement>(".rgb_color");
const hexPreview = document.querySelector<HTMLElement>(".hex_color");
const languageTogglers = document.querySelectorAll<HTMLElement>(".language_toggler");

let currentTab!: Awaited<ReturnType<typeof browser.tabs.query>>[number];
let currentTabImage!: HTMLImageElement;
let intervalCapture!: number;

async function initPopupScript() {
    try {
        await setCurrentTab();
        await insertContentScript();
        await insertContentStyle();
        setPopupLabels();

        startButton?.addEventListener("click", async () => {
            sendMessageToContent(currentTab.id as number, STAT_CAPTURE_ACTION);
            requestPermissions();

            intervalCapture = setInterval(setCurrentTabCapture, 600);
        });
        languageTogglers?.forEach((el) => {
            el.addEventListener("click", (e) => {
                const languageCode = (e.target as any).dataset.value;
                setPopupLabels(languageCode);
                sendMessageToContent(currentTab.id as number, LANGUAGE_CHANGE_ACTION, languageCode);
            });
        });

        browser.runtime.onMessage.addListener(async (message) => {
            if (message.action === MOUSE_MOVE_ACTION) {
                const result = await showPreview(message.payload.x, message.payload.y);
                sendMessageToContent(currentTab.id as number, NEW_COLOR_ACTION, result);
            }
        });
    } catch (error) {
        errorHandler(error);
    }
}
initPopupScript();

// ---------------------------------
function errorHandler(error: any) {
    console.error(`[GOLOR ERROR] ${error.message}`);

    clearInterval(intervalCapture);

    popupBody?.classList.add("hidden");
    errorContent?.classList.remove("hidden");
}

async function requestPermissions() {
    try {
        const granted = await browser.permissions.request({ origins: ["<all_urls>"] });
        if (!granted) throw new Error("Permissions not granted");
    } catch (error) {
        errorHandler(error);
    }
}

async function setCurrentTab() {
    const queryOptions = { active: true, lastFocusedWindow: true };

    const tabs = await browser.tabs.query(queryOptions);
    currentTab = tabs[0];
}

async function setCurrentTabCapture() {
    const dataUrl = await browser.tabs.captureVisibleTab();

    currentTabImage = document.createElement("img");
    currentTabImage.src = dataUrl;
}

function insertContentScript() {
    return browser.scripting.executeScript({
        target: { tabId: currentTab.id as number },
        files: [CONTENT_SCRIPT_PATH]
    });
}

function insertContentStyle() {
    return browser.scripting.insertCSS({
        target: { tabId: currentTab.id as number },
        files: [CONTENT_STYLE_PATH]
    });
}

function removeContentStyle() {
    return browser.scripting.removeCSS({
        target: { tabId: currentTab.id as number },
        files: [CONTENT_STYLE_PATH]
    });
}

function sendMessageToContent(tabId: number, action: string, payload = {}) {
    const view = browser.extension.getViews({ type: "popup" });
    if (view.length <= 0) return;

    const message = { action, payload };
    browser.tabs.sendMessage(tabId, message);
}

async function showPreview(x: number, y: number) {
    if (!(colorPreview && rgbPreview && hexPreview && currentTab && currentTabImage)) return;

    const canvas = document.createElement("canvas");
    canvas.height = currentTab.height || 0;
    canvas.width = currentTab.width || 0;

    const ctx = canvas.getContext("2d");
    if (!ctx) return;
    ctx.drawImage(currentTabImage, 0, 0, canvas.width, canvas.height);

    const imageData = ctx.getImageData(x, y, 1, 1);
    const data = imageData.data;

    const red = data[0];
    const green = data[1];
    const blue = data[2];

    const rgb = `${red}, ${green}, ${blue}`;
    const hex = rgb2hex(red, green, blue);

    colorPreview.style.background = `rgb(${rgb})`;
    rgbPreview.textContent = `RGB: ${rgb}`;
    hexPreview.textContent = `HEX: ${hex}`;

    return { hex, rgb };
}

function rgb2hex(r = 0, g = 0, b = 0) {
    const hr = getHexUnit(r);
    const hg = getHexUnit(g);
    const hb = getHexUnit(b);

    return `#${hr}${hg}${hb}`;
}
function getHexUnit(colorUnit = 0) {
    return Math.max(0, Math.min(255, Math.round(colorUnit)))
        .toString(16)
        .padStart(2, "0");
}

function setPopupLabels(languageCode: "en" | "fr" = "en") {
    if (!startButton) return;

    const label = popupLabelsMap[languageCode];

    startButton.textContent = label.data.sections.mainForm.buttons.pickColor.label;

    languageTogglers.forEach((el) => {
        if (el.dataset.value === languageCode) {
            return el.classList.add("active");
        }
        el.classList.remove("active");
    });
}

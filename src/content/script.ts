/**
 * - API istead of 'browser' to not interfere with the popup script 'browser'
 * - It is needed for the extention to work in both firefox and chromium based's browsers
 */
var API = typeof browser != "undefined" ? browser : chrome;

var STAT_CAPTURE_ACTION = "startcapture";
var MOUSE_MOVE_ACTION = "mousemove";
var NEW_COLOR_ACTION = "newcolor";
var LANGUAGE_CHANGE_ACTION = "languagechange";

const TARGERT_ELEMENT_CLASS = "golor_element";

const contentLabelsMap = {
    en: {
        languageCode2: "en",
        data: {
            pageTitle: "",
            sections: {
                notification: {
                    colorCopied: { title: "", message: "Color copied to clipboard !" }
                }
            }
        }
    },
    fr: {
        languageCode2: "fr",
        data: {
            pageTitle: "",
            sections: {
                notification: {
                    colorCopied: { title: "", message: "Couleur copiée dans le presse-papier !" }
                }
            }
        }
    }
};

let currentLanguage: "en" | "fr" = "en";
let currentColor = "";

async function initContentStript() {
    try {
        if (window.hasGolorRun) return;
        window.hasGolorRun = true;

        API.runtime.onMessage.addListener((message) => {
            if (message.action === STAT_CAPTURE_ACTION) {
                document.addEventListener("mousemove", mouseMoveEventHandler);
                document.addEventListener("click", clickEventHandler, true);

                return;
            }
            if (message.action === NEW_COLOR_ACTION) {
                currentColor = JSON.stringify(message.payload);
                return;
            }
            if (message.action === LANGUAGE_CHANGE_ACTION) {
                currentLanguage = message.payload;
                return;
            }
        });
    } catch (error) {
        console.error(error);
    }
}
initContentStript();

// ----------------------------------

function sendMessageToExtension(action: string, payload = {}) {
    const message = { action, payload };

    if (!API.runtime?.id) return;
    API.runtime.sendMessage(message);
}

function mouseMoveEventHandler(event: MouseEvent) {
    removeTargetElementClass();
    (event.target as any).classList.add(TARGERT_ELEMENT_CLASS);

    sendMessageToExtension(MOUSE_MOVE_ACTION, {
        x: event.clientX,
        y: event.clientY
    });
}

function clickEventHandler(event: MouseEvent) {
    event.stopPropagation();
    event.preventDefault();

    cleanUpEventHandlers();
    removeTargetElementClass();
    copyColor();
}
function cleanUpEventHandlers() {
    document.removeEventListener("mousemove", mouseMoveEventHandler);
    document.removeEventListener("click", clickEventHandler, true);
}

function removeTargetElementClass() {
    const elements = document.querySelectorAll(`.${TARGERT_ELEMENT_CLASS}`);
    elements.forEach((e) => e.classList.remove(TARGERT_ELEMENT_CLASS));
}

function copyColor() {
    alert(contentLabelsMap[currentLanguage].data.sections.notification.colorCopied.message);

    const tmpElement = document.createElement("input");
    document.body.append(tmpElement);

    window.navigator.clipboard.writeText(currentColor);
    currentColor = "";

    tmpElement.remove();
}
